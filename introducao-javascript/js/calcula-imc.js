var pacientes = document.querySelectorAll('.paciente');
pacientes.forEach(function(paciente) {
    var peso = paciente.querySelector('.info-peso').textContent;
    var altura = paciente.querySelector('.info-altura').textContent;
    if (!validaPeso(peso)) {
        paciente.querySelector('.info-imc').textContent = 'Peso inválido!';
        paciente.classList.add('paciente-invalido');
    } else if (!validaAltura(altura)) {
        paciente.querySelector('.info-imc').textContent = 'Altura inválida!';
        paciente.classList.add('paciente-invalido');
    } else {
        var imc = calculaImc(peso, altura);
        paciente.querySelector('.info-imc').textContent = imc;
    }
})

function validaPeso(peso) {
  if (peso > 0 & peso < 500) {
        return true;
  }
  return false;
}

function validaAltura(altura) {
  if (altura > 0 & altura < 3) {
        return true;
    }
  return false;
}

function calculaImc(peso, altura) {
  var imc = peso / (altura * altura);
  return imc.toFixed(2);
}