var pacientes = document.querySelectorAll('.paciente');

var tabela = document.querySelector('#tabela-pacientes');

tabela.addEventListener('dblclick', function(event) {
  var elemento = event.target.parentNode;
  elemento.classList.add('fadeOut');
  setTimeout(function() {
    elemento.remove();
  }, 300);
})