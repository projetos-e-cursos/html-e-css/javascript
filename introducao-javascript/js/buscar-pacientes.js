var botaoAdicionar = document.querySelector('#buscar-pacientes');

botaoAdicionar.addEventListener('click', function() {
  var endereco = 'https://api-pacientes.herokuapp.com/pacientes';
  
  var xhr = new XMLHttpRequest();
  
  xhr.open('GET', endereco);
  
  xhr.addEventListener('load', function() {
    if (xhr.status != 200) {
      console.log(xhr.status);
      console.log(xhr.responseText);
      return
    }
    
    var resposta = xhr.responseText;
    var pacientes = JSON.parse(resposta);
    
    pacientes.forEach(function(paciente) {
      adicionaPacienteNaTabela(paciente)
    })
    
  })
  
  xhr.send();
})